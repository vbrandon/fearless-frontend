
function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="text-secondary">
          <h6 class="card-subtitle mb-2 text-muted">${location} </h6>
          </p>
          <p class="card-text">${description}</p>
          <div class="card-footer">
          ${starts} - ${ends}
        </div>
        </div>
      </div>
    `;
  }







window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
    const response = await fetch(url);

    if (!response.ok) {
        window.onerror = function() {
            alert(`Invalid response!`);
            return true;

    }}


    else {
        const data = await response.json();

        for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts).toDateString();
            const ends = new Date(details.conference.ends).toDateString();
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            console.log('this is', html);
            const column = document.querySelector('.col');
            column.innerHTML += html;
        }
        }

    }
    } catch (e) {
        window.onerror = function() {
            alert(`An error has occured!`);
            return true;
        };

    }

});
